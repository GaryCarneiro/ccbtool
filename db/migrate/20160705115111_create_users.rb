class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :user_email
      t.text :user_teams
      t.string :user_manager

      t.timestamps
    end
  end
end
