json.array!(@teams) do |team|
  json.extract! team, :id, :team_name, :team_members, :team_manager
  json.url team_url(team, format: :json)
end
